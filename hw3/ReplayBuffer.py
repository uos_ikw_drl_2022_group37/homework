import numpy as np
import tensorflow as tf


class ReplayBuffer:

    def __init__(self, state_space, max_size=100000, batch_size=64):
        self.max_size = max_size
        self.batch_size = batch_size
        self.current_position = 0
        self.buffer_full = False
        self.s = np.zeros((self.max_size, state_space), dtype=np.float32)
        self.a = np.zeros(self.max_size, dtype=np.float32)
        self.r = np.zeros(self.max_size, dtype=np.float32)
        self.s_p = np.zeros((self.max_size, state_space), dtype=np.float32)
        self.d = np.zeros(self.max_size, dtype=int)
        self.un_norm_r = np.zeros(self.max_size, dtype=np.float32)

    def add(self, s, a, r, s_p, d):
        self.s[self.current_position] = s
        self.a[self.current_position] = a

        self.r[self.current_position] = r
        # self.un_norm_r[self.current_position] = r
        # self.r = (self.un_norm_r - np.mean(self.un_norm_r)) / (np.std(self.un_norm_r) + 1e-10)

        self.s_p[self.current_position] = s_p
        self.d[self.current_position] = d
        if self.current_position + 1 >= self.max_size:
            self.buffer_full = True
        self.current_position = (self.current_position + 1) % self.max_size

    def get_batch(self):
        pos = self.max_size if self.buffer_full else self.current_position
        batch_indexes = np.random.choice(pos, self.batch_size)
        states = tf.convert_to_tensor(self.s[batch_indexes], dtype=tf.float32)
        actions = tf.convert_to_tensor(self.a[batch_indexes], dtype=tf.int32)
        rewards = tf.convert_to_tensor(self.r[batch_indexes], dtype=tf.float32)
        states_p = tf.convert_to_tensor(self.s_p[batch_indexes], dtype=tf.float32)
        dones = tf.convert_to_tensor(self.d[batch_indexes], dtype=tf.float32)
        return states, actions, rewards, states_p, dones
