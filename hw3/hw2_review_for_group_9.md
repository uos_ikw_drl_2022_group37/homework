# 2 Homework review for group 9

Dear group 9,

your sollution to the homework looks very good.

### Task 1:
Your code looks very good and complete.
We very much liked the inclusion of the visualizations in the notebook.

Very good job.

### Task 2:
Your SARSA implementation also looks very nice and complete.
You structured your code very well and included comprehensible comments.


Overall we can only say, you did an outstanding job with this task.
We have nothing to critique here.
Keep up the good work.

Best,
group 37
