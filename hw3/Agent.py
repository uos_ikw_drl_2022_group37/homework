from ReplayBuffer import ReplayBuffer
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.optimizers import Adam
import numpy as np


class Agent:

    def __init__(self, environment, max_replay_buffer_size=100000, batch_size=64,
                 learning_rate=0.001, gamma=0.95, tau=0.05):
        self.action_space = 4  # TODO: dynamic not fixed 4
        self.state_space = 8  # TODO: dynamic not fixed 8
        self.reply_buffer = ReplayBuffer(self.state_space, max_size=max_replay_buffer_size, batch_size=batch_size)
        self.batch_size = batch_size
        self.environment = environment
        self.gamma = gamma
        self.tau = tau
        self.epsilon = 1
        self.learning_rate = learning_rate
        self.mse = tf.keras.losses.MeanSquaredError()
        self.q_network = self.create_q_network()
        self.t_q_network = self.create_q_network()

    def train(self, epochs, steps_per_epoch, iterations_per_step):
        print("start training!")
        rets = []
        for e in range(epochs):
            print("epoch:", e)
            print("avg ret:", np.average(rets), "epsilon:", self.epsilon)
            rets = []
            # self.update_weight() eigentlich hier ist aber zu selten :(
            for n in range(steps_per_epoch):
                self.update_weight()
                ret = self.sample_to_replay_buffer()
                rets.append(ret)
                # decay epsilon by 0.999 up to a min 0f 0.01
                self.epsilon = self.epsilon * 0.99 if self.epsilon > 0.01 else 0.01
                print(n, "return:", ret, "ret avg 100:", np.average(rets[-100:]))
                for k in range(iterations_per_step):
                    s, a, r, s_p, d = self.reply_buffer.get_batch()
                    self.train_step(s, a, r, s_p, d)
        print("training finished!")

    def train_step(self, s, a, r, s_p, d):  # TODO: ugly! replace numpy functions with tensorflow natives
        q_old = self.q_network(s)
        t = q_old.numpy()
        batch_index = np.arange(self.batch_size, dtype=np.int32)
        q_values = self.t_q_network(s_p)
        max_q = np.max(q_values, axis=-1)  # tf.reduce_max(q_values, axis=-1)
        t[batch_index, a] = r + (1 - d) * self.gamma * max_q  # (1-d) : no q if done
        self.q_network.train_on_batch(s, t)

    def train_step2(self, s, a, r, s_p, d):
        q_values = self.t_q_network(s_p)
        max_q = tf.reduce_max(q_values, axis=-1)  # .numpy()
        t = r + self.gamma * (1 - d) * max_q  # (1-d) : no q if done
        with tf.GradientTape() as tape:
            q_prev = self.q_network(s)
            prev = tf.gather(q_prev, a, batch_dims=-1)  # q values of actions a
            loss = self.mse(t, prev)
        gradients = tape.gradient(loss, self.q_network.trainable_variables)
        self.q_network.optimizer.apply_gradients(zip(gradients, self.q_network.trainable_variables))

    def update_weight(self):
        weights = []
        targets = self.t_q_network.weights
        for i, weight in enumerate(self.q_network.weights):
            weights.append((1 - self.tau) * targets[i] + self.tau * weight)
        self.t_q_network.set_weights(weights)

    def act(self, s):
        # e greedy
        if np.random.random() <= self.epsilon:
            a = np.random.choice(self.action_space)
        else:
            q = self.q_network(tf.convert_to_tensor([s], dtype=tf.float32))
            a = tf.math.argmax(tf.squeeze(q)).numpy().item()
            # draw from prob distribution over q values
            # a = tf.squeeze(tf.random.categorical(tf.math.log(q), 1)).numpy().item()
        s_p, r, d, _ = self.environment.step(a)
        return a, s_p, r, d

    def sample_to_replay_buffer(self):
        s = self.environment.reset()
        d = 0
        ret = 0
        while not d:
            a, s_p, r, d = self.act(s)
            ret += r
            self.reply_buffer.add(s, a, r, s_p, d)
            s = s_p
        return ret

    def create_q_network(self, state_dim, action_dim):
        inputs = keras.Input(shape=state_dim)
        x = layers.Dense(256, activation="relu")(inputs)
        x = layers.Dense(256, activation="relu")(x)
        out = layers.Dense(action_dim, activation=None)(x)
        model = keras.Model(inputs=inputs, outputs=out)
        model.compile(optimizer=Adam(learning_rate=self.learning_rate), loss=self.mse)
        return model
