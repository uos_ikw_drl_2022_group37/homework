from Agent import Agent
import gym

env = gym.make("LunarLander-v2")
env.reset()
agent = Agent(env)
agent.train(1000, 50, 50)
