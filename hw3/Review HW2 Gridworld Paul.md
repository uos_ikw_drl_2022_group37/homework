# Review HW2 Gridworld Paul

Nice variable name for `p = GridWorld.slippery`  :D

Your gridworld is not the prettiest but that was not the goal here. But to some degree this hinders easy understanding

You included all necessary element, that is good.

You even included docstrings, that is very nice in general you have a lot of good comments

`class IllegalFieldException(Exception):`good use of an exception



Over all a nice homework and good documented. Good job!
