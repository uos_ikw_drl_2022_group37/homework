# Review HW3 Group 16

## 1. Task: Experience replay buffer

- There is nothing to critic from my side functionality wise, you could have written the doc-string inside of the class. 

- Otherwise very nice implementation of the Experience replay buffer. :+1: 

## 2. Task & 3. Task:

- Your implementation is very eleborate

- Your use of doc-strings gives a good structure into your code

- Since you did it beforehand you could have put the training into a function to make it callable

- Unconventional but very handy for the homework, that you commited the output of the notebook to github :+1:

- good visualization of the training 

- Good & complete implementation



**Good job!** :blush: 


