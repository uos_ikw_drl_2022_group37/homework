from EpisodeBuffer import EpisodeBuffer
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow import keras
from tensorflow.keras.layers import Conv2D, Dense, Flatten, BatchNormalization, GlobalAvgPool2D, Rescaling
from tensorflow.keras.optimizers import Adam
import numpy as np


class Agent:

    def __init__(self, environment, learning_rate=0.0003, gamma=0.99):
        self.environment = environment
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.policy_network = self.create_policy_network()

    def train(self, epochs):
        print("start training!")
        rets = []
        for e in range(epochs):
            print("epoch:", e)
            buffer, ret = self.sample_to_episode_buffer()
            rets.append(ret)
            print("return of episode:", ret, "avg 100:", np.average(rets[-100:]))
            episode = buffer.get_as_data_set()
            with tf.GradientTape() as tape:
                loss = 0
                for s, a, _, g in episode:
                    policy = self.policy_network(s)
                    prob_of_a = self.log_prob_of_action(policy, a)
                    loss += -(g * prob_of_a)
            gradients = tape.gradient(loss, self.policy_network.trainable_variables)
            self.policy_network.optimizer.apply_gradients(zip(gradients, self.policy_network.trainable_variables))
        print("training finished!")

    def log_prob_of_action(self, policy, action):
        action_probs = self.distribution_of_oplicy(policy)
        log_prob = action_probs.log_prob(action)
        return log_prob

    def distribution_of_policy(self, policy):
        mus, sigmas = policy
        return tfp.distributions.MultivariateNormalDiag(loc=mus, scale_diag=sigmas)

    def act(self, s):
        policy = self.policy_network(tf.convert_to_tensor([s], dtype=tf.float32))
        dist = self.distribution_of_oplicy(policy)
        a = tf.squeeze(dist.sample())
        actions = self.scale_actions(a)
        s_p, r, d, _ = self.environment.step(actions)
        return a, s_p, r, d

    def scale_actions(self, a):
        actions = tf.clip_by_value(a, 0, 1).numpy()  # clipping not necessary with sigmoid
        actions = [actions[0] * 2 - 1, actions[1], actions[2]]
        return actions

    def sample_to_episode_buffer(self):
        buffer = EpisodeBuffer(self.gamma)
        s = self.environment.reset()
        d = 0
        ret = 0
        while not d:
            a, s_p, r, d = self.act(s)
            ret += r
            buffer.add(s, a, r)
            s = s_p
        return buffer, ret

    def create_policy_network(self):
        inputs = keras.Input(shape=(96, 96, 3))
        x = Rescaling(1. / 127.5, offset=-1)(inputs)
        x = Conv2D(filters=32, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=32, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        x = Conv2D(filters=64, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=64, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        x = Conv2D(filters=128, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=128, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        # x = Flatten()(x)
        x = GlobalAvgPool2D()(x)
        out = (Dense(3, activation=tf.nn.sigmoid)(x),
               Dense(3, activation=tf.nn.softplus)(x))  # (mu, sigma), or covariance matrix for sigma
        model = keras.Model(inputs=inputs, outputs=out)
        model.compile(optimizer=Adam(learning_rate=self.learning_rate))
        return model
