import tensorflow as tf
import numpy as np


class EpisodeBuffer:

    def __init__(self, gamma):
        self.gamma = gamma
        self.s = []
        self.a = []
        self.r = []

    def add(self, s, a, r):
        self.s.append(tf.convert_to_tensor(s, dtype=tf.float32))
        self.a.append(tf.convert_to_tensor(a, dtype=tf.float32))
        self.r.append(tf.convert_to_tensor(r, dtype=tf.float32))

    def get_as_data_set(self):
        g = np.zeros_like(self.r, dtype=np.float32)  # TODO: better implementation for discounting (cumsum, tf.scan)
        for t in range(len(self.r)):
            g_sum = 0
            gamma_t = 1
            for k in range(t, len(self.r)):
                g_sum += self.r[k] * gamma_t  # r_n[k].numpy()
                gamma_t *= self.gamma
            g[t] = g_sum
        # g = (g - np.mean(g)) / (np.std(g) + 1e-10) # normalize g?
        return tf.data.Dataset.from_tensor_slices((self.s, self.a, self.r, g)).batch(1)

    def get_episode_from_timestep(self, t):
        return zip(self.s[t:], self.a[t:], self.r[t:])
