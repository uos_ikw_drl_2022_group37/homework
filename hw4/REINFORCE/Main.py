from Agent import Agent
import gym

env = gym.make("CarRacing-v1")
env.reset()
agent = Agent(env)
agent.train(2000)
