import tensorflow as tf
import numpy as np


class EpisodeBuffer:

    def __init__(self, gamma):
        self.gamma = gamma
        self.s = []
        self.a = []
        self.r = []
        self.adv = []
        self.p = []

    def add(self, s, a, r, adv, p):
        self.s.append(tf.convert_to_tensor(s, dtype=tf.float32))
        self.a.append(tf.convert_to_tensor(a, dtype=tf.float32))
        self.r.append(tf.convert_to_tensor(r, dtype=tf.float32))
        self.adv.append(tf.convert_to_tensor(adv, dtype=tf.float32))
        self.p.append(tf.convert_to_tensor(p, dtype=tf.float32))

    def get_as_data_set(self, batch_size=1):
        g = np.zeros_like(self.r, dtype=np.float32)  # TODO: better implementation for discounting (cumsum, tf.scan)
        for t in range(len(self.r)):
            g_sum = 0
            gamma_t = 1
            for k in range(t, len(self.r)):
                g_sum += self.r[k] * gamma_t  # r_n[k].numpy()
                gamma_t *= self.gamma
            g[t] = g_sum
        # g = (g - np.mean(g)) / (np.std(g) + 1e-10) # normalize g?
        return tf.data.Dataset.from_tensor_slices((self.s, self.a, self.r, g, self.adv, self.p)).shuffle(
            np.min(1000, self.size())).batch(batch_size)

    def get_episode_from_timestep(self, t):
        return zip(self.s[t:], self.a[t:], self.r[t:])

    def size(self):
        return len(self.a)

    def generalized_adv_estimate(self, lam):  # not finished
        d = []  # dones missing
        v = []  # v missing: v = [self.value_network(s) for s in self.s]
        advantages = np.zeros(len(self.r), dtype=np.float32)
        for t in range(len(self.r) - 1):
            discount = 1
            a_t = 0
            for k in range(t, len(self.r) - 1):
                a_t += discount * (self.r[k] + self.gamma * v[k + 1] * (1 - d[k]) - v[k])
                discount *= self.gamma * lam
            advantages[t] = a_t
        return advantages
