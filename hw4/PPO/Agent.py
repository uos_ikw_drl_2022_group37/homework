from EpisodeBuffer import EpisodeBuffer
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow import keras
from tensorflow.keras.layers import Conv2D, Dense, Flatten, BatchNormalization, GlobalAvgPool2D, Rescaling
from tensorflow.keras.optimizers import Adam
import numpy as np


class Agent:

    def __init__(self, environment, learning_rate=0.0003, gamma=0.99, epsilon=0.3):
        self.environment = environment
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.epsilon = epsilon
        self.mse = tf.keras.losses.MeanSquaredError()
        self.policy_network = self.create_policy_network()
        self.value_network = self.create_value_network()

    def train(self, epochs):
        print("start training!")
        rets = []
        for e in range(epochs):
            print("epoch:", e)
            buffer, ret = self.sample_to_episode_buffer()
            rets.append(ret)
            print("return of episode:", ret, "avg 100:", np.average(rets[-100:]))
            self.train_epoch_step2(buffer)
        print("training finished!")

    def train_epoch_step(self, buffer):
        episode = buffer.get_as_data_set()
        with tf.GradientTape() as tape:  # TODO: for one batch?
            loss = 0
            for s, a, _, _, adv, prob in episode:
                policy = self.policy_network(s)
                prob_of_a = self.log_prob_of_action(policy, a)
                # prob of current policy / prob of old policy (log probs: p/p2 = log(p)-log(p2)
                p = tf.math.exp(prob_of_a - prob)  # exp() to un do log(p)
                value = p * adv
                clipped_value = tf.clip_by_value(p, 1 - self.epsilon, 1 + self.epsilon) * adv
                loss += -tf.math.minimum(value, clipped_value)  # tf.math.reduce_mean(loss)
        gradients = tape.gradient(loss, self.policy_network.trainable_variables)
        self.policy_network.optimizer.apply_gradients(zip(gradients, self.policy_network.trainable_variables))

        d_size = buffer._size()
        with tf.GradientTape() as tape:
            loss = 0
            for s, _, _, r_sum, adv, _ in episode:
                prev_v = self.value_network(s)
                loss += self.mse(r_sum, prev_v)
            loss = (1 / d_size) * loss
        gradients = tape.gradient(loss, self.value_network.trainable_variables)
        self.value_network.optimizer.apply_gradients(zip(gradients, self.value_network.trainable_variables))

    def kullback_leibler_divergence(self, x, y):
        return np.sum(x[i] * np.log(x[i] / y[i]) for i in range(len(x)))

    def train_epoch_step2(self, buffer):  # TODO: early stopping with Kullback Leibler divergence
        kld_threshold = 0  # TODO
        episode = buffer.get_as_data_set(batch_size=32)
        for s, a, _, r_sum, adv, prob in episode:
            with tf.GradientTape() as tape:
                policy = self.policy_network(s)
                prob_of_a = self.log_prob_of_action(policy, a)
                # prob of current policy / prob of old policy (log probs: p/p2 = log(p)-log(p2)
                p = tf.math.exp(prob_of_a - prob)  # exp() to un do log(p)
                value = p * adv
                clipped_value = tf.clip_by_value(p, 1 - self.epsilon, 1 + self.epsilon) * adv
                loss = -tf.math.reduce_mean(tf.math.minimum(value, clipped_value))
            gradients = tape.gradient(loss, self.policy_network.trainable_variables)
            self.policy_network.optimizer.apply_gradients(zip(gradients, self.policy_network.trainable_variables))

            with tf.GradientTape() as tape:
                prev_v = self.value_network(s)
                loss = self.mse(r_sum, prev_v)
            gradients = tape.gradient(loss, self.value_network.trainable_variables)
            self.value_network.optimizer.apply_gradients(zip(gradients, self.value_network.trainable_variables))
            kld = 0  # TODO
            if kld > kld_threshold:
                break

    def log_prob_of_action(self, policy, action):
        action_probs = self.distribution_of_oplicy(policy)
        log_prob = action_probs.log_prob(action)
        return log_prob

    def distribution_of_oplicy(self, policy):
        mus, sigmas = policy
        return tfp.distributions.MultivariateNormalDiag(loc=mus, scale_diag=sigmas)

    def act(self, s):
        policy = self.policy_network(tf.convert_to_tensor([s], dtype=tf.float32))
        dist = self.distribution_of_oplicy(policy)
        a = tf.squeeze(dist.sample())
        actions = self.scale_actions(a)
        s_p, r, d, _ = self.environment.step(actions)
        p = dist.log_prob(a)
        return a, s_p, r, d, p

    def scale_actions(self, a):
        actions = tf.clip_by_value(a, 0, 1).numpy()  # clipping not necessary with sigmoid
        actions = [actions[0] * 2 - 1, actions[1], actions[2]]
        return actions

    def sample_to_episode_buffer(self):
        buffer = EpisodeBuffer(self.gamma)
        s = self.environment.reset()
        d = 0
        ret = 0
        while not d:
            a, s_p, r, d, p = self.act(s)
            ret += r
            adv = self.estimate_advantage(s, s_p, r)
            buffer.add(s, a, r, adv, p)
            s = s_p
        return buffer, ret

    def estimate_advantage(self, s, s_p, r):
        v = tf.squeeze(self.value_network(tf.convert_to_tensor([s], dtype=tf.float32)))
        v_p = tf.squeeze(self.value_network(tf.convert_to_tensor([s_p], dtype=tf.float32)))
        return r + self.gamma * v_p - v

    def create_policy_network(self):
        inputs = keras.Input(shape=(96, 96, 3))
        x = Rescaling(1. / 127.5, offset=-1)(inputs)
        x = Conv2D(filters=16, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=16, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        x = Conv2D(filters=32, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=32, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        x = Conv2D(filters=64, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=64, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        # x = Flatten()(x)
        x = GlobalAvgPool2D()(x)
        out = (Dense(3, activation=tf.nn.sigmoid)(x),  # (mu, sigma), or covariance matrix for sigma
               Dense(3, activation=tf.nn.softplus)(
                   x))  # softplus or sigmoid with scaling or set to fixed sigma (or with cooling schedule)
        model = keras.Model(inputs=inputs, outputs=out)
        model.compile(optimizer=Adam(learning_rate=self.learning_rate))
        return model

    def create_value_network(self):
        inputs = keras.Input(shape=(96, 96, 3))
        x = Rescaling(1. / 127.5, offset=-1)(inputs)
        x = Conv2D(filters=16, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=16, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        x = Conv2D(filters=32, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=32, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        x = Conv2D(filters=64, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
        x = Conv2D(filters=64, kernel_size=3, strides=2, padding='same', activation=tf.nn.relu)(x)
        x = BatchNormalization()(x)
        # x = Flatten()(x)
        x = GlobalAvgPool2D()(x)
        out = Dense(1, activation=None)(x)
        model = keras.Model(inputs=inputs, outputs=out)
        model.compile(optimizer=Adam(learning_rate=self.learning_rate))
        model.summary()
        return model
