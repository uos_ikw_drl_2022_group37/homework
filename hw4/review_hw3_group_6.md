# DRL Homework 3 Review - Group 6

Dear group 6,

here comes our review of your third homework sheet.
As an overall point, some comments would have been nice, to be able to follow the code better, but as most of the code is relatively straight forward to understand, it is also not that important.

### Task 1
Your solution here looks neat and compact, you have implemented all the necessary parts, well done.

### Task 2
Similarly to task 1, your solution here looks good (maybe here some comments would have been nice :D ). You have implemented all the necessary parts, well done.

### Task 3
Your solution looks good, however some graphical display of the training and results would have been nice here.

Overall, we really liked your sollution. Keep up the good work.

Best wishes,
group 37
