# Review Homework 03 - Group 04

Dear group 4,


## Task 1
Your solution to the task looks very good. It is well structured and you included nice comments. Well done.

## Task 2
We have nothing to critique here, it looks complete and well structured. Well done.

## Task 3
Similarly to the other two tasks, we also think your solution is very good.
Your code is well structured, includes good comments and links and implements all the necessary parts of the task and specifically the training here.
You also included a nice text based visualization. 
(As an suggestion, maybe a graph could have been nice here.)

Overall we can only say, well done. Keep up the good work :D

Best wishes,

Group 37
