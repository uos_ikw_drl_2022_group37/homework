# HW1

## 1 Task 01

> You are tasked with creating an AI for the game of chess. To solve the problem 
> using Reinforcement Learning, you have to frame the game of chess as a Markov Decision Process (MDP). Describe both the game of chess formally as a MDP, also formalize the respective policy

---

> * the set of states $S$ -> finite or infinite, with states $s∈S$
> * the set of Actions $A$ -> finite or infinite, with actions $a∈A$
> * the probabilistic state dynamics  $p(s′|s,a)$ -> the probability of ending up in state s' when taking action a in state s (formal: $p(St+1|St,At)$ )
> * the (probabilistic) reward dynamics $p(Rt+1|s,a)$, often as  $(s,a)=E[rt+1|s,a]$(the expected value of the reward given state and action)
> * optional: initial state distribution $μ$ or $p0$ over $S$
> 
> The agent interacts with the Environment via the so called policy $π$:
> 
> * $π(a|s)$ to be read as: The probability of taking action $a∈A$ when encountering the state $s∈S$
> 
> * This is a probability distribution over actions given a state. A
>   good policy has very large probabilities for good actions and low
>   probability for bad actions given the state
> 
> * Specifically defining $π(a|s)$ for every possible state and action fully describes the policy!
> 
> * Notice: While generally we have probabilistic policies, we can set
>   the probability of one action to 1, the probability of all other actions
>    to 0, thereby creating a deterministic policy   
>   
>   [From UOS_IKW_DRL_2022]

- For simplicity, we will assume that we contain our environment by the [rules of chess](https://en.wikipedia.org/wiki/Rules_of_chess), e.g. making moving the knight straight instead of diagonal impossible. 

- Hence, the environment is defined as the chess board underlying all chess rules

- The set of states is defined by all combinations of how to place the chess pieces (legally) on the board. 

- The initial setup of the board can be regarded as the start state

- The possible actions $A$ can be described by all legal moves in chess.
  
  **Policy:** 
  
  The policy determines which move to play. 
  
  The policy decides between all possible actions ( legal moves) in a given state based on the state (placement of pieces on the board). 
  
  The policy might be modeled as a probability distribution as a joint distribution of Normal distributions. Each one denoting a possible action.

## 2 Task 02

> Check out the LunarLander environment on OpenAI Gym: [Check out this Link!](https://gym.openai.com/envs/LunarLander-v2/).  Describe the environment as a MDP, include a description how the policy is formalized.  

--- 

- The set of states is defined by all positions the LunarLander can be in (i.e. the coordinates or pixel in space, excluding the area of the ground).

- The set of possible actions is defined by all directions in 360° the Lander can go in via the three thrusters on its exterior, excluding directions or areas where the lander would move into the ground. So the direct actions would be activating any combination of thrusters (inlcluding none). 

- The initial start can be described as the position the Lander starts at and the borders of the ground, defined by the height-map.

- The transition function can be described as a combination of the position of the Lander, the previous movement direction/thruster speed and the height-map of the ground.

- The reward to calculate the next best action can be calculated via the distance between the Lunar to the landing spot.

**Policy:**

The policy for picking the next action. In this case the policy is formalized as a probability distribution of the current state of the Lander described by the position of the Lander, the distance to the goal, the rotation and the current amount of achieved points and a set of possible action the Lander can take (direction of movement, decided by thruster engines).

## 3 Task 03

> Discuss the Policy Evaluation and Policy Iteration algorithms from the lecture.  They explicitly make use of the environment dynamics $(p(s′, r|s, a))$.  
> 
> - Explain what the environment dynamics (i.e. reward function and state  transition function) are and give at least two examples. 
> - Discuss: Are the environment dynamics generally known and can practically be used to solve a problem with RL

---

Computing the state-value function for an arbitrary policy is called policy evaluation. Policy improvement is the process of improving a policy to get an optimal policy. Policy improvement can be achieved by the policy iteration algorithm. Policy iteration uses policy evaluation to compute the state values for a given policy and then choses the best policy for each state. This iteratively continues until the policies don’t change any more (convergence). 

$p(s′, r|s, a)$ gives the probability for transitioning to state $s’$ with the reward function $r$ given state $s$ and action $a$. In a grid world there could be tiles that should be visited (or end up in). Every tile would be a state. End tiles would get a reward expressed by their reward function $r$. From one tile you can enter another (state transition). Transitions are done according to a given policy $π$ (probabilities for state transitions to neighbor states).

In grid world environment dynamics are known and can be used to solve the MDP. In real world applications like self driving cars circumstances are different environment dynamics are not generally available. We could in general make a distinction between the observed state and the world state. Where the observed state includes all information the agent has about the world in that state and the world state being the complete description of the world.
