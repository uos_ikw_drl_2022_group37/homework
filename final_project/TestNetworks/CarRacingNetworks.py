import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv2D, Concatenate, Add, Dense, BatchNormalization, MaxPool2D, GlobalAvgPool2D, \
    Rescaling
from tensorflow.keras.optimizers import Adam
import numpy as np


def car_racing_scaling(actions):
    a = tf.squeeze(actions).numpy()
    a = a + np.array([0, 1, 1])
    a = a * np.array([1, 0.5, 0.5])
    return a


def create_policy_network(learning_rate, action_dim=3):
    inputs = keras.Input(shape=(96, 96, 3))
    x = Rescaling(1. / 127.5, offset=-1)(inputs)
    x = Conv2D(filters=16, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
    x = MaxPool2D(pool_size=2, strides=2)(x)
    x = BatchNormalization()(x)
    x = Conv2D(filters=32, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
    x = MaxPool2D(pool_size=2, strides=2)(x)
    x = BatchNormalization()(x)
    x = Conv2D(filters=64, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
    x = MaxPool2D(pool_size=2, strides=2)(x)
    x = BatchNormalization()(x)
    x = GlobalAvgPool2D()(x)
    mu = Dense(action_dim, activation=None)(x)
    sigma = Dense(action_dim, activation=tf.nn.softplus)(x)
    model = keras.Model(inputs=inputs, outputs=(mu, sigma))
    model.compile(optimizer=Adam(learning_rate=learning_rate))
    return model


def create_q_network(learning_rate, action_dim=3):
    inputs_s = keras.Input(shape=(96, 96, 3))
    x = Rescaling(1. / 127.5, offset=-1)(inputs_s)
    x = Conv2D(filters=16, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
    x = MaxPool2D(pool_size=2, strides=2)(x)
    x = BatchNormalization()(x)
    x = Conv2D(filters=32, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
    x = MaxPool2D(pool_size=2, strides=2)(x)
    x = BatchNormalization()(x)
    x = Conv2D(filters=64, kernel_size=3, padding='same', activation=tf.nn.relu)(x)
    x = MaxPool2D(pool_size=2, strides=2)(x)
    x = BatchNormalization()(x)
    x = GlobalAvgPool2D()(x)

    inputs_a = keras.Input(shape=action_dim)
    y = Dense(64, activation=tf.nn.relu)(inputs_a)

    x_y = Concatenate()([x, y])
    x_y = Dense(128)(x_y)

    out = Dense(1, activation=None)(x_y)
    model = keras.Model(inputs=(inputs_s, inputs_a), outputs=out)
    model.compile(optimizer=Adam(learning_rate=learning_rate))
    return model
