from Agent import Agent
from Gridworld import Gridworld

if __name__ == '__main__':
    world = Gridworld()
    player = Agent(epsilon=.1, alpha=.2, world=world)
    print("Training started")
    for iteration in range(1, 5000):
        player.n_step_sarsa(iteration=iteration, epsilon=100, gamma=0.5, alpha=0.5, n=5)
    print("Training done")
    ret = player.follow_optimal_policy()
    print("Reward is", ret[0])
    world.visualize()
    world.print_path(ret[1])
