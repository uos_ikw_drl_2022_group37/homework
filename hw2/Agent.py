from Gridworld import Gridworld
import numpy as np


class Agent:
    def __init__(self, epsilon, alpha, world: Gridworld):
        self.epsilon = epsilon
        self.alpha = alpha
        self.world = world
        self.states = np.full(world.get_world_dimensions(), -1, dtype=float)
        self.states[self.world.get_final_state()[0], self.world.get_final_state()[1]] = 1
        self.q = self.initialize_q(self.world.get_possible_states())

    def initialize_q(self, states):
        q = {}
        for s in states:
            q[s] = {}
            possible_moves = self.world.get_possible_moves(s)
            for a in possible_moves:
                q[s][a] = 0
        return q

    def max_q(self, state):
        max_value = -99999
        max_action = "up"
        for k, v in self.q[state].items():
            if v > max_value:
                max_value = v
                max_action = k
        return max_action

    def get_move_epsilon_greedy(self, epsilon, position):
        possible_moves = self.world.get_possible_moves(self.world.current_position)
        rand = np.random.rand()
        if rand < epsilon:
            move = possible_moves[np.random.choice(len(possible_moves))]
        else:
            move = self.max_q(position)
        return move

    def n_step_sarsa(self, iteration, gamma=0.5, alpha=0.5, epsilon=1, n=1, visualize=False):
        self.world.reset()
        n_steps = []
        current_position = self.world.current_position[:]
        current_move = self.get_move_epsilon_greedy(epsilon / iteration, current_position)
        while True:
            move = current_move
            while True:
                if visualize is True:
                    self.world.visualize()
                if len(n_steps) >= n or self.world.is_final_position():
                    break
                reward = self.world.step(move)[1]
                next_position = self.world.current_position[:]
                next_move = self.get_move_epsilon_greedy(epsilon / iteration, next_position)
                n_steps.append((reward, self.q[next_position][next_move], next_position, next_move))
                move = next_move
            q_old = self.q[current_position][current_move]
            e = sum([(pow(gamma, step) * r) for ((r, _, _, _), step) in zip(n_steps[:-1], range(1, len(n_steps)))]) \
                + n_steps[-1][1] - q_old
            # n step SARSA update rule
            self.q[current_position][current_move] = q_old + alpha * e
            current_position, current_move = n_steps[0][2:]
            n_steps = n_steps[1:]
            if self.world.is_final_position() and len(n_steps) == 0:
                break
        return 1

    def follow_optimal_policy(self):
        self.world.reset()
        rewards = []
        visiting = np.zeros(self.world.get_world_dimensions())
        t = 0
        while True:
            if t > 1000:
                print("training failed")
                break
            t += 1
            move = self.get_move_epsilon_greedy(epsilon=0, position=self.world.current_position[:])
            tmp = self.world.step(move)[:-1]
            visiting[tmp[0][0], tmp[0][1]] = t
            rewards.append(tmp[1])
            if self.world.is_final_position():
                break
        reward = np.sum(rewards)
        return reward, visiting
