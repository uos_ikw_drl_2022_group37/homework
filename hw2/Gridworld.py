import numpy as np


class Gridworld:
    def __init__(self):
        self.WORLD_WIDTH = 10
        self.WORLD_HEIGHT = 10
        self.START_POSITION = (0, 0)
        self.FINAL_POSITION = (self.WORLD_HEIGHT - 1, self.WORLD_WIDTH - 1)
        self.current_position = self.START_POSITION
        self.World_cord_all = [(i, j) for i in range(0, self.WORLD_WIDTH) for j in range(0, self.WORLD_HEIGHT)]
        self.BLOCKED_TRAPPED_RIGGED = (10, 10, 10)

        while True:
            self.special_tiles = [self.START_POSITION, self.FINAL_POSITION]
            self.blocked_tiles = self.world_wo_some(self.special_tiles)
            np.random.shuffle(self.blocked_tiles)
            self.blocked_tiles = self.blocked_tiles[:np.max(
                (self.BLOCKED_TRAPPED_RIGGED[0], np.random.randint(0, np.min((self.WORLD_WIDTH, self.WORLD_HEIGHT)))))]
            self.special_tiles += self.blocked_tiles

            self.trapped_tiles = self.world_wo_some(self.special_tiles)
            np.random.shuffle(self.trapped_tiles)
            self.trapped_tiles = self.trapped_tiles[:np.max(
                (self.BLOCKED_TRAPPED_RIGGED[1], np.random.randint(0, np.min((self.WORLD_WIDTH, self.WORLD_HEIGHT)))))]
            self.special_tiles += self.trapped_tiles

            self.rigged_tiles = self.world_wo_some(self.special_tiles)
            np.random.shuffle(self.rigged_tiles)
            self.rigged_tiles = self.rigged_tiles[:np.max(
                (self.BLOCKED_TRAPPED_RIGGED[2], np.random.randint(0, np.min((self.WORLD_WIDTH, self.WORLD_HEIGHT)))))]
            self.special_tiles += self.rigged_tiles

            if (len(self.adjacent_tiles(self.FINAL_POSITION)) > self.num_of_identical(
                    self.adjacent_tiles(self.FINAL_POSITION), self.blocked_tiles)) and (
                    len(self.adjacent_tiles(self.FINAL_POSITION)) > self.num_of_identical(
                self.adjacent_tiles(self.FINAL_POSITION), self.trapped_tiles)) and (
                    len(self.adjacent_tiles(self.FINAL_POSITION)) > self.num_of_identical(
                self.adjacent_tiles(self.FINAL_POSITION), self.rigged_tiles)):
                if (len(self.adjacent_tiles(self.START_POSITION)) > self.num_of_identical(
                        self.adjacent_tiles(self.START_POSITION), self.blocked_tiles)) and (
                        len(self.adjacent_tiles(self.START_POSITION)) > self.num_of_identical(
                    self.adjacent_tiles(self.START_POSITION), self.trapped_tiles)) and (
                        len(self.adjacent_tiles(self.START_POSITION)) > self.num_of_identical(
                    self.adjacent_tiles(self.START_POSITION), self.rigged_tiles)):
                    # if len(np.unique((self.blocked_tiles + self.trapped_tiles + self.rigged_tiles))) == len((self.blocked_tiles + self.trapped_tiles + self.rigged_tiles)):
                    # if 0 == (self.num_of_identical(self.blocked_tiles,self.trapped_tiles) + self.num_of_identical(self.blocked_tiles,self.rigged_tiles) + self.num_of_identical(self.rigged_tiles,self.trapped_tiles)):
                    if self.START_POSITION not in (self.blocked_tiles + self.trapped_tiles + self.rigged_tiles):
                        if self.FINAL_POSITION not in (self.blocked_tiles + self.trapped_tiles + self.rigged_tiles):
                            break

    def world_wo_some(self, some):
        return [(i, j) for i in range(0, self.WORLD_WIDTH) for j in range(0, self.WORLD_HEIGHT) if (i, j) not in some]

    def is_final_position(self):
        return self.current_position == self.FINAL_POSITION

    def num_of_identical(self, x, y):
        num = 0
        for _ in x:
            if x in y:
                num += 1
        return num

    def step(self, move):
        new_position = list(self.current_position)
        if move == "up":
            new_position[0] = self.current_position[0] - 1
        elif move == "down":
            new_position[0] = self.current_position[0] + 1
        elif move == "left":
            new_position[1] = self.current_position[1] - 1
        elif move == "right":
            new_position[1] = self.current_position[1] + 1
        else:
            raise Exception("move not known")
        new_position = tuple(new_position)

        if new_position in self.blocked_tiles or new_position[0] not in range(0, self.WORLD_HEIGHT) or new_position[
            1] not in range(0, self.WORLD_WIDTH):
            new_position = self.current_position
        if new_position in self.rigged_tiles and np.random.rand() < 0.7:
            if (new_position[0] + 1, new_position[1]) in self.adjacent_tiles((new_position[0] + 1, new_position[1])):
                new_position = (new_position[0] + 1, new_position[1])

        self.current_position = new_position

        if self.is_final_position():
            reward = 10
        elif self.current_position in self.trapped_tiles:
            reward = -10
        else:
            reward = -1
        return self.current_position, reward, self.is_final_position()

    def visualize(self):
        dim = self.WORLD_HEIGHT, self.WORLD_WIDTH
        blocked = self.blocked_tiles
        traps = self.trapped_tiles
        rigged = self.rigged_tiles
        print("----------------------------------------")
        for i in range(dim[0]):
            for j in range(dim[1]):
                if self.FINAL_POSITION == (i, j):
                    print("{!}", end="\t")
                elif (i, j) == self.current_position:
                    print("@", end="\t")
                elif (i, j) in blocked:
                    print("#", end="\t")
                elif (i, j) in traps:
                    print("_", end="\t")
                elif (i, j) in rigged:
                    print("+", end="\t")
                else:
                    print(".", end="\t")
            print()
        print("----------------------------------------")



    def print_path(self, visiting):
        dim = self.get_world_dimensions()
        blocked, trapped, rigged = self.get_blocked_trapped_rigged()
        print("Path taken:")
        print("----------------------------------------")
        for i in range(dim[0]):
            for j in range(dim[1]):
                if self.current_position == (i, j):
                    print("{!}", end="\t")
                elif self.get_start() == (i, j):
                    print("{s}", end="\t")
                elif (i, j) in blocked:
                    print("#", end="\t")
                elif (i, j) in trapped:
                    print("_", end="\t")
                elif (i, j) in rigged:
                    print("+", end="\t")
                else:
                    print('{:1.0f}'.format(visiting[(i, j)]), end="\t")
            print()
        print("----------------------------------------")
        print()

    def adjacent_tiles(self, tile):
        return [i for i in self.World_cord_all if (
                i != tile and (i[0] == tile[0] and (i[1] == tile[1] - 1 or i[1] == tile[1] + 1)) or (
                i[1] == tile[1] and (
                i[0] == tile[0] - 1 or i[0] == tile[0] + 1))) and i not in self.blocked_tiles]

    def get_possible_moves(self, state):
        possible_tiles = self.adjacent_tiles(state)
        states_check = [(state[0] + 1, state[1]), (state[0] - 1, state[1]), (state[0], state[1] - 1),
                        (state[0], state[1] + 1)]
        moves = {0: "down", 1: "up", 2: "left", 3: "right"}
        possible_moves = [moves[i] for i in range(len(states_check)) if states_check[i] in possible_tiles]
        return possible_moves

    def get_world_dimensions(self):
        return self.WORLD_HEIGHT, self.WORLD_WIDTH

    def get_blocked_trapped_rigged(self):
        return self.blocked_tiles, self.trapped_tiles, self.rigged_tiles

    def reset(self):
        self.current_position = self.START_POSITION

    def get_possible_states(self):
        states = self.world_wo_some(self.blocked_tiles)
        return states

    def get_final_state(self):
        return self.FINAL_POSITION

    def get_start(self):
        return self.START_POSITION
