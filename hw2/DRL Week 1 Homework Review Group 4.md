## DRL homework Nr.1 review of group 4

Dear group 4,

you did very well with your submission/homework of the first week.

Below, we have added a few points regarding each of the tasks.
We hope they are understandable. However, we also don't claim full knowledge of the "correct" solution,
so maybe take everything with a grain of salt :D

#### Task 1:
We think your solution is quite nice and incorporates all the necessary parts of an MDP.
You also gave a good formal description of the MDP.
Only regarding the policy, we think that the policy can incorporate more than just capturing the king, as also reducing
the other pieces of the opponent could be included here.
But besides that, you did a great job.

#### Task 2:
In this task, you did quite a good job describing the LunarLander.
Nothing to add here.

#### Task 3:
We think you chose nice examples for this task and explained the reward function quite well.
The only thing we could critique, was that you could have explained the state-transition function a bit more explicitly.
But otherwise, great job.


Keep up the great work for the next weeks.

Best wishes,
Group 37
